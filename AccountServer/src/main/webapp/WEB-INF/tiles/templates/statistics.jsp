<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="statistics" class="panel panel-default">
    <s:message var="title" code="jsp.statistics.title"/>
    <s:message var="total_read_operations_text" code="jsp.statistics.total_read_operatons"/>
    <s:message var="total_write_operations_text" code="jsp.statistics.total_write_operations"/>
    <s:message var="read_operations_in_sec_text" code="jsp.statistics.read_operations_in_sec"/>
    <s:message var="write_operations_in_sec_text" code="jsp.statistics.write_operations_in_sec"/>

    <div class="panel-heading">
        <p class="text-center">${title}</p>
    </div>

    <table class="table">
        <tr>
            <th>${total_read_operations_text}</th>
            <td id="number-of-read-operations">${numberOfReadOperations}</td>
        </tr>
        <tr>
            <th>${total_write_operations_text}</th>
            <td id="number-of-write-operations">${numberOfWriteOperations}</td>
        </tr>
        <tr>
            <td colspan="2" class="empty-cell"/>
        </tr>
        <tr>
            <th>${read_operations_in_sec_text}</th>
            <td id="read-operations-in-sec">${readOperationsInSec}</td>
        </tr>
        <tr>
            <th>${write_operations_in_sec_text}</th>
            <td id="write-operations-in-sec">${writeOperationsInSec}</td>
        </tr>
    </table>

    <div class="panel-footer">
        <s:message var="refresh_button_text" code="jsp.statistics.button.refresh.text"/>
        <button id="refresh" class="btn btn-primary">${refresh_button_text}</button>

        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <s:message var="reset_button_text" code="jsp.statistics.button.reset.text"/>
            <button id="reset" class="btn btn-primary">${reset_button_text}</button>
        </sec:authorize>
    </div>
</div>

