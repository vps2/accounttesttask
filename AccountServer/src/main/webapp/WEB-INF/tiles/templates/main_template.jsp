<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="_csrf" content="${_csrf.token}"/>
        <!-- default header name is X-CSRF-TOKEN -->
        <meta name="_csrf_header" content="${_csrf.headerName}"/>

        <s:message var="title" code="jsp.main_template.title"/>
        <title>${title}</title>

        <s:url var="resources_path" value="/resources"/>
        <c:set var="compress_mode" value="${isJsCompressed}"/>
        <c:set var="uri_string" value="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}"/>
        <c:set var="queryl_params" value="${pageContext.request.getAttribute('javax.servlet.forward.query_string')}"/>

        <c:choose>
            <c:when test="${compress_mode eq 'true'}">
                <link href="${resources_path}/wro/css/main.css" rel="stylesheet" type="text/css"/>

                <!-- HTML5 shim and Respond.js in one js file for IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                <script src="${resources_path}/wro/scripts/libs_for_bootstrap_ie8_support.js"><jsp:text/></script>
                <![endif]-->
            </c:when>
            <c:otherwise>
                <link href="${resources_path}/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
                <link href="${resources_path}/css/app/account-service.css" rel="stylesheet" type="text/css"/>

                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                <script src="${resources_path}/scripts/lib/html5shiv.min.js"><jsp:text/></script>
                <script src="${resources_path}/scripts/lib/respond.min.js"><jsp:text/></script>
                <![endif]-->
            </c:otherwise>
        </c:choose>
    </head>

    <body>
        <div class="container-fluid">
            <header>
                <t:insertAttribute name="header"/>
            </header>
            <main>
                <t:insertAttribute name="body"/>
            </main>
        </div>

        <%--Подгрузка скриптов--%>
        <c:choose>
            <c:when test="${compress_mode eq 'true'}">
                <script src="${resources_path}/wro/scripts/main.js" type="text/javascript"><jsp:text/></script>

                <c:if test="${fn:contains(uri_string, 'home')}">
                    <script src="${resources_path}/wro/scripts/statistics.js" type="text/javascript"><jsp:text/></script>
                </c:if>
                <c:if test="${fn:contains(uri_string, 'users') and empty queryl_params}">
                    <script src="${resources_path}/wro/scripts/users.js" type="text/javascript"><jsp:text/></script>
                </c:if>
                <c:if test="${fn:contains(uri_string, 'users') and
                              (fn:contains(queryl_params, 'new') or fn:contains(queryl_params, 'update'))
                             }">
                    <script src="${resources_path}/wro/scripts/user_profile.js" type="text/javascript"><jsp:text/></script>
                </c:if>
            </c:when>
            <c:otherwise>
                <script src="${resources_path}/scripts/lib/jquery/jquery-1.11.3.min.js" type="text/javascript"><jsp:text/></script>
                <script src="${resources_path}/scripts/lib/jquery/jquery-ui.min.js" type="text/javascript"><jsp:text/></script>

                <script src="${resources_path}/scripts/lib/bootstrap/bootstrap.min.js" type="text/javascript"><jsp:text/></script>

                <script src="${resources_path}/scripts/app/application.js" type="text/javascript"><jsp:text/></script>
                <script src="${resources_path}/scripts/app/login.js" type="text/javascript"><jsp:text/></script>
                <c:if test="${fn:contains(uri_string, 'home')}">
                    <script src="${resources_path}/scripts/app/statistics.js" type="text/javascript"><jsp:text/></script>
                </c:if>
                <c:if test="${fn:contains(uri_string, 'users') and empty queryl_params}">
                    <script src="${resources_path}/scripts/app/users.js" type="text/javascript"><jsp:text/></script>
                </c:if>
                <c:if test="${fn:contains(uri_string, 'users') and
                              (fn:contains(queryl_params, 'new') or fn:contains(queryl_params, 'update'))
                             }">
                    <script src="${resources_path}/scripts/app/user_profile.js" type="text/javascript"><jsp:text/></script>
                </c:if>
            </c:otherwise>
        </c:choose>
    </body>
</html>