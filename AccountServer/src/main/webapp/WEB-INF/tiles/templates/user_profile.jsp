<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-default">
    <s:message var="title" code="jsp.user_profile.title"/>
    <s:message var="label_name" code="jsp.user_profile.label.name"/>
    <s:message var="label_password" code="jsp.user_profile.label.password"/>
    <s:message var="label_role" code="jsp.user_profile.label.role"/>
    <s:message var="button_submit" code="jsp.user_profile.button.submit"/>
    <s:message var="button_cancel" code="jsp.user_profile.button.cancel"/>

    <c:choose>
        <c:when test="${user.id == null}">
            <s:url var="action_url" value="/users?new"/>
        </c:when>
        <c:otherwise>
            <s:url var="action_url" value="/users/${user.id}?update"/>
        </c:otherwise>
    </c:choose>

    <div class="panel-heading">
        <p class="text-center">${title}</p>
    </div>

    <div class="panel-body">
        <sf:form id="user-profile-form" cssClass="form-horizontal" method="post" modelAttribute="user" action="${action_url}">
            <div class="form-group">
                <sf:hidden path="id"/>
            </div>
            <div class="form-group">
                <sf:label path="name" cssClass="control-label col-sm-3">${label_name}</sf:label>
                <div class="col-sm-9">
                    <sf:input path="name" id="name" cssClass="form-control input-sm" autofocus="autofocus"/>
                </div>
                <div class="col-sm-offset-3 col-sm-9">
                    <sf:errors path="name" cssClass="help-block alert alert-danger"/>
                </div>
            </div>
            <div class="form-group">
                <sf:label path="password" cssClass="control-label col-sm-3">${label_password}</sf:label>
                <div class="col-sm-9">
                    <sf:password path="password" id="password" showPassword="false" cssClass="form-control input-sm"/>
                    <sf:hidden path="passwordChanged"/>
                </div>
                <div class="col-sm-offset-3 col-sm-9">
                    <sf:errors path="password" cssClass="help-block alert alert-danger"/>
                </div>
            </div>
            <div class="form-group">
                <sf:label path="role" cssClass="control-label col-sm-3">${label_role}</sf:label>
                <c:forEach var="available_role" items="${allRoles}">
                    <div class="col-sm-2">
                        <label class="checkbox-inline">
                            <c:choose>
                                <c:when test="${available_role == 'ROLE_USER'}">
                                    <sf:radiobutton path="role" value="${available_role}" checked="checked"/>
                                </c:when>
                                <c:otherwise>
                                    <sf:radiobutton path="role" value="${available_role}"/>
                                </c:otherwise>
                            </c:choose>
                            ${available_role}
                        </label>
                    </div>
                </c:forEach>
                <div class="col-sm-offset-3 col-sm-9">
                    <sf:errors path="role" cssClass="help-block alert alert-danger"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-xs-9">
                    <s:url var="users_page_url" value="/users"/>
                    <input type="submit" class="btn btn-warning" value="${button_submit}"/>
                    <input type="button" id="cancel-button" class="btn btn-default" value="${button_cancel}"
                           data-target-url="${users_page_url}"/>
                </div>
            </div>
        </sf:form>
    </div>
</div>