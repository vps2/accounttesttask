<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
    <head>
        <s:message var="title" code="jsp.error.title"/>

        <title>Ошибка</title>

        <s:url value="/resources" var="resources_path" />
        <link href="${resources_path}/css/account-service.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <s:message var="h1_text" code="jsp.error.h1.text"/>
        <s:message var="error_text" code="jsp.error.text"/>

        <h1 class="error">${h1_text}</h1>
        <p class="error"><strong>${error_text}</strong></p>

        <!--
        Failed URL: ${url}
        Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">    ${ste}
        </c:forEach>
        -->
    </body>
</html>
