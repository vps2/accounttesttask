<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div id="users" class="panel panel-default">
    <s:message var="title" code="jsp.users.title"/>
    <s:message var="user_id_header" code="jsp.users.user_id_header"/>
    <s:message var="user_name_header" code="jsp.users.user_name_header"/>
    <s:message var="user_role_header" code="jsp.users.user_role_header"/>
    <s:message var="user_action_header" code="jsp.users.user_action_header"/>
    <s:message var="button_delete_text" code="jsp.users.button.delete.text"/>
    <s:message var="button_change_text" code="jsp.users.button.change.text"/>
    <s:message var="button_new_text" code="jsp.users.button.new.text"/>

    <div class="panel-heading">
        <p class="text-center">${title}</p>
    </div>

    <table id="users" class="table">
        <thead>
            <th class="hidden">${user_id_header}</th>
            <th>${user_name_header}</th>
            <th>${user_role_header}</th>
            <th>${user_action_header}</th>
        </thead>
        <tbody>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td class="hidden" name="user-id">
                        <c:out value="${user.id}"/>
                    </td>
                    <td name="user-name">
                        <c:out value="${user.name}"/>
                    </td>
                    <td name="user-role">
                        <c:out value="${user.role}"/>
                    </td>
                    <td>
                        <s:url var="update_user_url" value="/users/${user.id}?update"/>

                        <c:set var="delete_form_name" value="delete-user${user.id}-form"/>
                        <s:url var="delete_user_url" value="/users/${user.id}?delete"/>

                        <form id="${delete_form_name}" method="post" action="${delete_user_url}">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>

                        <button id="update-user${user.id}-button" class="btn btn-warning"
                                data-target-url="${update_user_url}">${button_change_text}</button>
                        <button id="delete-user${user.id}-button" class="btn btn-danger"
                                data-submit-form="${delete_form_name}">${button_delete_text}</button>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <div class="panel-footer">
        <s:url var="new_user_url" value="/users?new"/>
        <button id="new-user-button" class="btn btn-primary" data-target-url="${new_user_url}">${button_new_text}</button>
    </div>
</div>