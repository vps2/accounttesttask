<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="hello" class="panel-body">
    <s:message var="body_text" code="jsp.default_body.text"/>
    <h3 class="text-center text-danger">${body_text}</h3>
</div>