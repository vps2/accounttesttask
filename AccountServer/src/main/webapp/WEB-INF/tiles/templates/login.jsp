<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <sec:authorize access="!isAuthenticated()">
            <s:message var="label_username" code="jsp.login.label.username"/>
            <s:message var="label_password" code="jsp.login.label.password"/>
            <s:message var="button_text" code="jsp.login.button.text"/>
            <s:message var="error_msg" code="jsp.login.error"/>

            <s:url var="auth_url" value="/login"/>
            <form id="credentials-form" class="navbar-form navbar-left" method="post" action="${auth_url}">
                <div class="form-group">
                    <label class="sr-only control-label" for="username">${label_username}</label>
                    <input type="text" class="form-control input-sm" name="_username" id="username" placeholder="${label_username}"
                           autofocus="autofocus">
                </div>
                <div class="form-group">
                    <label class="sr-only control-label" for="password">${label_password}</label>
                    <input type="password" class="form-control input-sm" name="_password" id="password" placeholder="${label_password}">
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                <input type="submit" class="btn btn-primary" name="submit" value="${button_text}"/>
            </form>
            <c:if test="${param.error != null}">
                <div id="login-error" class="navbar-right">
                    <p class="navbar-text text-center alert-danger">${error_msg}</p>
                </div>
            </c:if>
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">
            <s:message var="link_logout_text" code="jsp.login.link.logout"/>
            <s:message var="link_admin_page_text" code="jsp.login.link.admin_page"/>
            <s:message var="link_statistics_page_text" code="jsp.login.link.statistics_page"/>

            <s:url var="logout_url" value="/logout"/>
            <s:url var="users_url" value="/users"/>
            <s:url var="statistics_url" value="/home"/>

            <form id="logout-form" class="navbar-form navbar-left" method="post" action="${logout_url}">
                <div class="form-group">
                    <p class="navbar-text">
                        <b><sec:authentication property="principal.username"/></b>
                        <a id="logout-link" class="navbar-link" href="">${link_logout_text}</a>
                    </p>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
            </form>

            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <ul id="menu" class="nav navbar-nav navbar-right">
                    <li>
                        <a id="statistics-link" href="${statistics_url}">${link_statistics_page_text}</a>
                    </li>
                    <li>
                        <a id="admin-link" href="${users_url}">${link_admin_page_text}</a>
                    </li>
                </ul>
            </sec:authorize>
        </sec:authorize>
    </div>
</nav>