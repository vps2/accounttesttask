$(document).ready(function () {

    var queryURL = "home/statistics";

    $("button[id='refresh']").on("click", function () {
        $(this).blur();

        $.getJSON(queryURL, function(data) {
            updateStatistics(data);
        });
    });

    $("button[id='reset']").on("click", function () {
        $(this).blur();

        //Метод put вызывается для обнуления статистики.
        $.ajax({
            method: "put",
            url: queryURL,
            success: function() {
                $("button[id='refresh']").trigger('click')
            }
        });
    });

    function updateStatistics(json) {
        var jsonString = JSON.stringify(json);
        JSON.parse(jsonString, function (key, value) {
            var elementId = key.replace(/([A-Z])/g, "-$1").toLowerCase();
            $("td[id=" + elementId + "]").text(value);
        });
    }
});