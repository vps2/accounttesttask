$(document).ready(function() {
    if(isElementExists("#logout-form")) {
        $("#logout-link").on("click", function(event) {
            event.preventDefault();

            $("#logout-form").trigger("submit");
        });

        $("ul#menu li").removeClass("active");
        $("ul#menu a[href='" + window.location.pathname + "']").parent().addClass("active");
    }

    if(isElementExists("div#login-error")) {
        $("#navbarCollapse").addClass("in");
    }
    else {
        $("#navbarCollapse").removeClass("in");
    }
});