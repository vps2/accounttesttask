$(document).ready(function() {
    if(isElementExists("form[id^='delete-user'][id$='-form']")) {
        $("button[id^='delete-user'][id$='-button']").on("click", function(event) {
            event.preventDefault();

            var formId = $(this).data("submit-form");
            $("#" + formId).trigger("submit");
        });

        $("button[id^='update-user'][id$='-button'], button#new-user-button").on("click", goToURL);
    }
});