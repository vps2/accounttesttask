$(document).ready(function() {
    if(isElementExists("form#user-profile-form")) {
        if(getUrlVars().indexOf("update") != -1) {
            var fakePassword = password(5);

            var $passwordInput = $("input#password");
            $passwordInput.val(fakePassword);

            var $passwordChangedInput = $('input#passwordChanged').val(false);

            $("form#user-profile-form").submit(function() {
                if($passwordInput.val() !== fakePassword)
                {
                    $passwordChangedInput.val(true);
                }
            });
        }


        $("input#cancel-button").on("click", goToURL);
    }

    function getUrlVars()
    {
        return window.location.href.slice(window.location.href.indexOf('?') + 1).split(/[&?]{1}[\w\d]+=/);
    }

    function password(length, special) {
        var iteration = 0;
        var password = "";
        var randomNumber;
        if(special == undefined){
            var special = false;
        }
        while(iteration < length){
            randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
            if(!special){
                if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
                if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
                if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
                if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
            }
            iteration++;
            password += String.fromCharCode(randomNumber);
        }
        return password;
    }
});