var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

//Для того, чтобы исключить CSRF атаку при ajax запросах.
$(document).ajaxSend(function(e, xhr, options) {
    xhr.setRequestHeader(header, token);
});

$(document).ready(function() {
    //Автофокус для элементов ввода браузеров, не поддерживающих HTML5
    $("[autofocus]").filter(":visible:not(:hidden):focusable:not(:focus)").eq(0).focus();
});

function goToURL(event) {
    event.preventDefault();

    var url = $(this).data("target-url");
    window.location.href = url;
}

function isElementExists(element) {
    return $(element).length ? true : false;
}