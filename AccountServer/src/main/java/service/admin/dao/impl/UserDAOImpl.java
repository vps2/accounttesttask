package service.admin.dao.impl;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.stereotype.Repository;
import service.admin.dao.UserDAO;
import service.admin.domain.User;
import service.common.dao.AbstractGenericDaoJpa;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository("userDao")
public class UserDAOImpl extends AbstractGenericDaoJpa<User, Long> implements UserDAO
{
    @PersistenceContext(unitName = "admin-service")
    @Override
    protected void setEntityManager(EntityManager entityManager)
    {
        super.setEntityManager(entityManager);
    }

    @PostConstruct
    private void init()
    {
        if(getEntityManager() == null)
        {
            throw new BeanCreationException("Entity manager can not be a null");
        }
    }
}
