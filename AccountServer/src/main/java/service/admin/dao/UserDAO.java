package service.admin.dao;

import service.admin.domain.User;
import service.common.dao.GenericDao;

public interface UserDAO extends GenericDao<User, Long>
{
}
