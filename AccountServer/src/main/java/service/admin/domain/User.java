package service.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class User
{
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Role getRole()
    {
        return role;
    }

    public void setRole(Role role)
    {
        this.role = role;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }

        User user = (User) o;

        if(name != null ? !name.equals(user.name) : user.name != null)
        {
            return false;
        }
        if(password != null ? !password.equals(user.password) : user.password != null)
        {
            return false;
        }
        return role == user.role;
    }

    @Override
    public int hashCode()
    {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "User{" +
               "id=" + id +
               "; name='" + name + '\'' +
               "; password='" + password + '\'' +
               "; role=" + role +
               '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false, length = 15)
    @NotNull
    @Size(min = 3, max = 15, message = "{validation.user.name.size}")
    @Pattern(regexp = "^[a-zA-Z]+_?[a-zA-Z0-9]+$", message = "{validation.user.name.pattern}")
    private String name;

    @Column(nullable = false, length = 100)
    @NotNull
    @Size(min = 5, max = 100, message = "{validation.user.password.size}")
    private String password;

    @Column(nullable = false)
    @NotNull(message = "{validation.user.role}")
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER;
}
