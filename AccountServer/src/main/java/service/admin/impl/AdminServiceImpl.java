package service.admin.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.admin.AdminService;
import service.admin.dao.UserDAO;
import service.admin.domain.Role;
import service.admin.domain.User;

import java.util.Arrays;
import java.util.List;

@Secured("ROLE_ADMIN")
@Transactional(transactionManager = "adminServiceTransactionManager"
		                , propagation = Propagation.REQUIRED
		                , isolation = Isolation.READ_COMMITTED
		                , readOnly = true)
@Service("adminService")
public class AdminServiceImpl implements AdminService
{
	@Autowired
	public AdminServiceImpl(UserDAO userDao)
	{
		this.userDao = userDao;
	}

	@Autowired(required = false)
	public void setPasswordEncoder(PasswordEncoder encoder)
	{
		this.passwordEncoder = encoder;
	}

	@Override
	public List<Role> getRoles()
	{
		return Arrays.asList(Role.values());
	}

	@Override
	public List<User> getUsers()
	{
		return userDao.findAll();
	}

	@Override
	public User getUser(Long id)
	{
		return userDao.findByID(id);
	}

	@Transactional(transactionManager = "adminServiceTransactionManager", readOnly = false)
	@Override
	public void addUser(User user)
	{
		if(user == null)
		{
			return;
		}

		encodePassword(user);

		userDao.persist(user);
	}

	@Transactional(transactionManager = "adminServiceTransactionManager", readOnly = false)
	@Override
	public void updateUser(User user)
	{
		if(user == null)
		{
			return;
		}

		User originalUser = userDao.findByID(user.getId());
		originalUser.setName(user.getName());
		originalUser.setRole(user.getRole());
		if(!"".equals(user.getPassword()))
		{
			encodePassword(user);
			originalUser.setPassword(user.getPassword());
		}

		userDao.update(originalUser);
	}

	@Transactional(transactionManager = "adminServiceTransactionManager", readOnly = false)
	@Override
	public void deleteUser(User user)
	{
		if(user == null)
		{
			return;
		}

		User userToBeRemoved = userDao.findByID(user.getId());
		userDao.remove(userToBeRemoved);
	}

	private void encodePassword(User user)
	{
		if(passwordEncoder != null)
		{
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}
	}

	private UserDAO userDao;
	//
	private PasswordEncoder passwordEncoder;
}
