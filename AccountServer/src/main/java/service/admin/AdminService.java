package service.admin;

import service.admin.domain.Role;
import service.admin.domain.User;

import java.util.List;

public interface AdminService
{
	List<Role> getRoles();
	List<User> getUsers();
	User getUser(Long id);
	void addUser(User user);
	void updateUser(User user);
	void deleteUser(User user);
}
