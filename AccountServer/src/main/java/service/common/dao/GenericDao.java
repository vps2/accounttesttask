package service.common.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<E, K extends Serializable>
{
    /**
     * Сохраняет новый экземпляр сущности в хранилище.
     * @param entity экземпляр новой сущности.
     */
    void persist(E entity);
    /**
     * Обновляет экземпляр существующей сущности в хранилище.
     * @param entity экземпляр обновляемой сущности.
     */
    void update(E entity);
    /**
     * Удаляет экземпляр сущности хранилища.
     * @param entity экземпляр удаляемой сущности.
     */
    void remove(E entity);
    /**
     * Возвращает список всех сущностей заданного типа.
     * @return список сущностей.
     */
    List<E> findAll();
    /**
     * Получает экземпляр сущности из хранилища по идентификатору.
     * @param id идентификатор сущности в хранилище.
     * @return экземпляр сущности или null, если такой не существует.
     */
    E findByID(K id);
}
