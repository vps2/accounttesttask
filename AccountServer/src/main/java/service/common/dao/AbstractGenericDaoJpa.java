package service.common.dao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractGenericDaoJpa<E, K extends Serializable> implements GenericDao<E, K>
{
    public AbstractGenericDaoJpa()
    {
        ParameterizedType genericSuperClass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityType = (Class) genericSuperClass.getActualTypeArguments()[0];
    }

    @Override
    public void persist(E entity)
    {
        entityManager.persist(entity);
    }

    @Override
    public void update(E entity)
    {
        entityManager.merge(entity);
    }

    @Override
    public void remove(E entity)
    {
        entityManager.remove(entity);
    }

    @Override
    public List<E> findAll()
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entityType);

        Root<E> root = criteriaQuery.from(entityType);
        criteriaQuery.select(root);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    protected void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    protected EntityManager getEntityManager()
    {
        return entityManager;
    }

    @Override
    public E findByID(K id)
    {
        return entityManager.find(entityType, id);
    }

    private EntityManager entityManager;
    private Class<E> entityType;
}
