package service.account.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.account.AccountService;
import service.account.dao.AccountDao;
import service.account.domain.Account;

import javax.persistence.LockModeType;

@Service(value = "accountService")
public class AccountServiceImpl implements AccountService
{
   @Autowired
   public AccountServiceImpl(AccountDao accountDao)
   {
      this.accountDao = accountDao;
   }

   @Transactional(transactionManager = "accountServiceTransactionManager"
           , propagation = Propagation.REQUIRED
           , isolation = Isolation.READ_COMMITTED
           , readOnly = false)
   @Override
   public Long getAmount(Integer id)
   {
      Account account = getAccountFor(id, LockModeType.PESSIMISTIC_READ);

      if(account == null)
      {
         return DEFAULT_VALUE;
      }

      return account.getBalance();
   }

   @Transactional(transactionManager = "accountServiceTransactionManager"
           , propagation = Propagation.REQUIRED
           , isolation = Isolation.READ_COMMITTED
           , readOnly = false)
   @Override
   public void addAmount(Integer id, Long value)
   {
//      String format = "%s\tTrying get lock for account with id: %s. Value: %d";
//      System.err.println(String.format(format, Thread.currentThread(), id, value));
      Account account = getAccountFor(id, LockModeType.PESSIMISTIC_WRITE);

      if(account == null)
      {
//         format = "%s\tTrying get synchronized block for account with id: %s";
//         System.err.println(String.format(format, Thread.currentThread(), id));
         synchronized(this)
         {
//            format = "%s\tTrying get lock for account with id: %s. Value: %d in synchronized block";
//            System.err.println(String.format(format, Thread.currentThread(), id, value));
            account = getAccountFor(id, LockModeType.PESSIMISTIC_WRITE);

            if(account == null)
            {
               account = createAccount(id);
               persistAccount(account);
            }

            updateAccountOnAmount(account, value);
         }
      }
      else
      {
         updateAccountOnAmount(account, value);
      }

//      format = "%s\tExit from method. Account with id: %s";
//      System.err.println(String.format(format, Thread.currentThread(), id, value));
   }

   private Account createAccount(Integer id)
   {
      Account account;
      account = new Account();
      account.setId(id);

      return account;
   }

   private Account getAccountFor(Integer id, LockModeType operation)
   {
      return accountDao.findByID(id, operation);
   }

   private void persistAccount(Account account)
   {
      accountDao.persist(account);
   }

   private void updateAccountOnAmount(Account account, Long amount)
   {
      if(amount < 0)
      {
         account.withdraw(Math.abs(amount));
      }
      else
      {
         account.deposit(amount);
      }

      accountDao.update(account);
   }

   private static final Long DEFAULT_VALUE = 0L;
   //
   private AccountDao accountDao;
}
