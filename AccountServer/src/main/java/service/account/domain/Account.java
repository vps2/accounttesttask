package service.account.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
public class Account implements Serializable
{
   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public long getBalance()
   {
      return balance;
   }

   /**
    * Добавляет заданную сумму к счёту.
    * @param amount сумма добавления
    * @exception  IllegalArgumentException если значение аргумента amount <= 0.
    */
   public void deposit(long amount)
   {
      if(amount <= 0)
      {
         throw new IllegalArgumentException();
      }

      balance += amount;
   }

   /**
    * Снимает заданную сумму со счёта.
    * @param amount сумма снятия
    * @exception  IllegalArgumentException если сумма снятия больше, чем есть на счёте или значение аргумента amount <= 0.
    */
   public void withdraw(long amount)
   {
      long result = balance - amount;
      if(amount <= 0 || result < 0)
      {
         throw new IllegalArgumentException();
      }

      balance = result;
   }

   @Override
   public String toString()
   {
      return "Account{" +
             "id=" + id +
             ", balance=" + balance +
             '}';
   }

   private static final long serialVersionUID = 1L;
   //
   @Id
   private int id;

   @Min(0)
   @Column(name = "BALANCE")
   private long balance = 0L;
}
