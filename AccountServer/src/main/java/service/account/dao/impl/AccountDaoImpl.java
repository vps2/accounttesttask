package service.account.dao.impl;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.stereotype.Repository;
import service.account.dao.AccountDao;
import service.account.domain.Account;
import service.common.dao.AbstractGenericDaoJpa;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

@Repository("accountDao")
public class AccountDaoImpl extends AbstractGenericDaoJpa<Account, Integer> implements AccountDao
{
	@Override
	public Account findByID(Integer id, LockModeType lockModeType)
	{
		return getEntityManager().find(Account.class, id, lockModeType);
	}

	@PersistenceContext(unitName = "account-service")
	@Override
	protected void setEntityManager(EntityManager entityManager)
	{
		super.setEntityManager(entityManager);
	}

	@PostConstruct
	private void init()
	{
		if(getEntityManager() == null)
		{
			throw new BeanCreationException("Entity manager can not be a null");
		}
	}
}
