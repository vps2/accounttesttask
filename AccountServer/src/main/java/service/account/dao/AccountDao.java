package service.account.dao;

import service.account.domain.Account;
import service.common.dao.GenericDao;

import javax.persistence.LockModeType;

public interface AccountDao extends GenericDao<Account, Integer>
{
    Account findByID(Integer id, LockModeType lockModeType);
}
