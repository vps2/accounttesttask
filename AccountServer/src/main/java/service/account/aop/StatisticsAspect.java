package service.account.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.statistics.StatisticsAggregator;

@Component
@Aspect
public class StatisticsAspect
{
	@Pointcut("execution(Long service.account.impl.AccountServiceImpl.getAmount(Integer))")
	public void getAmount()
	{
	}

	@Pointcut("execution(void service.account.impl.AccountServiceImpl.addAmount(Integer, Long))")
	public void addAmount()
	{
	}

	@Before("getAmount()")
	public void loggingOfGetAmountCalling()
	{
		statisticsAggregator.updateNumberOfReadOperations();
	}

	@Before("addAmount()")
	public void loggingOfAddAmountCalling()
	{
		statisticsAggregator.updateNumberOfWriteOperations();
	}

	@Autowired
	private StatisticsAggregator statisticsAggregator;
}
