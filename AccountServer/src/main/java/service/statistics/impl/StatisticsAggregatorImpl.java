package service.statistics.impl;

import org.springframework.stereotype.Component;
import service.statistics.StatisticsAggregator;

import java.util.concurrent.atomic.LongAdder;

/**
 * Потокобезопасный класс сбора статистики операций чтения и записи.
 */

@Component(value = "statisticsAggregator")
public class StatisticsAggregatorImpl implements StatisticsAggregator
{
	public void resetStatistics()
	{
		readOperations.reset();
		writeOperations.reset();
	}

	public void updateNumberOfReadOperations()
	{
		readOperations.increment();
	}

	public void updateNumberOfWriteOperations()
	{
		writeOperations.increment();
	}

	public long getTotalReadOperations()
	{
		return readOperations.sum();
	}

	public long getTotalWriteOperations()
	{
		return writeOperations.sum();
	}

	LongAdder readOperations = new LongAdder();
	LongAdder writeOperations = new LongAdder();
}
