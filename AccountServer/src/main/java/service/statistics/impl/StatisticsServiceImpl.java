package service.statistics.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import service.statistics.StatisticsAggregator;
import service.statistics.StatisticsService;

import javax.annotation.PostConstruct;

@Service(value = "statisticsService")
public class StatisticsServiceImpl implements StatisticsService
{
	@Autowired
	public StatisticsServiceImpl(StatisticsAggregator aggregator, Poller poller)
	{
		this.aggregator = aggregator;
		this.poller = poller;
	}

	@Override
	public long getNumberOfReadOperationsInSec()
	{
		return poller.getNumberOfReadOperationsInSec();
	}

	@Override
	public long getNumberOfWriteOperationsInSec()
	{
		return poller.getNumberOfWriteOperationsInSec();
	}

	@Override
	public long getTotalReadOperations()
	{
		return aggregator.getTotalReadOperations();
	}

	@Override
	public long getTotalWriteOperations()
	{
		return aggregator.getTotalWriteOperations();
	}

	@Secured("ROLE_ADMIN")
	@Override
	public void resetStatistics()
	{
		aggregator.resetStatistics();
		poller.resetStatistics();
	}

	@PostConstruct
	private void beanInitialization()
	{
		Thread thread = new Thread(poller);
		thread.setDaemon(true);
		thread.start();
	}

	private StatisticsAggregator aggregator;
	private Poller poller;
}
