package service.statistics.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
import service.statistics.StatisticsAggregator;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@ManagedResource(objectName = "AccountServer:name=Poller", description = "AccountServer Statistics Poller")
@Component(value = "poller")
public class Poller implements Runnable
{
    public Poller(StatisticsAggregator aggregator)
    {
        this(aggregator, DEFAULT_POLL_INTERVAL);
    }

    @Autowired
    public Poller(StatisticsAggregator aggregator, @Value("15000") int pollInterval)
    {
        this.aggregator = aggregator;
        this.pollInterval.set(pollInterval);
    }

    public long getNumberOfReadOperationsInSec()
    {
        return roInSec.get();
    }

    public long getNumberOfWriteOperationsInSec()
    {
        return woInSec.get();
    }

    public void resetStatistics()
    {
        roInSec.set(0L);
        woInSec.set(0L);
    }

    @ManagedAttribute
    public int getPollInterval()
    {
        return pollInterval.get();
    }

    @ManagedAttribute
    public void setPollInterval(int pollInterval)
    {
        this.pollInterval.set(pollInterval);
        pollIntervalChanged.set(true);
    }

    @Override
    public void run()
    {
        while(!Thread.currentThread().isInterrupted())
        {
            long numberOfReadOperationsAtBeginning = aggregator.getTotalReadOperations();
            long numberOfWriteOperationsAtBeginning = aggregator.getTotalWriteOperations();

            try
            {
                Thread.sleep(pollInterval.get());
            }
            catch(InterruptedException ex)
            {
                Thread.currentThread().interrupt();
            }

            long numberOfReadOperations = aggregator.getTotalReadOperations() - numberOfReadOperationsAtBeginning;
            long numberOfWriteOperations = aggregator.getTotalWriteOperations() - numberOfWriteOperationsAtBeginning;
            if(numberOfReadOperations < 0 || numberOfWriteOperations < 0 || pollIntervalChanged.get() == true)
            {
                pollIntervalChanged.set(false);
                continue;
            }

            roInSec.set((numberOfReadOperations * SECOND_FACTOR) / pollInterval.get());
            woInSec.set((numberOfWriteOperations * SECOND_FACTOR) / pollInterval.get());
        }
    }

    private final static int SECOND_FACTOR = 1000;
    private final static int DEFAULT_POLL_INTERVAL = 30000;

    private StatisticsAggregator aggregator;
    private AtomicInteger pollInterval = new AtomicInteger();
    private AtomicBoolean pollIntervalChanged = new AtomicBoolean(false);
    private AtomicLong roInSec = new AtomicLong();
    private AtomicLong woInSec = new AtomicLong();
}
