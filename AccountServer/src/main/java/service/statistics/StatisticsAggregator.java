package service.statistics;

public interface StatisticsAggregator
{
    long getTotalReadOperations();
    long getTotalWriteOperations();
    void updateNumberOfReadOperations();
    void updateNumberOfWriteOperations();
    void resetStatistics();
}
