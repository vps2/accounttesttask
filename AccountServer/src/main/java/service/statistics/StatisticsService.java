package service.statistics;

public interface StatisticsService
{
	long getNumberOfReadOperationsInSec();
	long getNumberOfWriteOperationsInSec();
	long getTotalReadOperations();
	long getTotalWriteOperations();
	void resetStatistics();
}
