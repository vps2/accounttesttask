package mvc.controller.dto;

import service.admin.domain.Role;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto
{
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Role getRole()
    {
        return role;
    }

    public void setRole(Role roles)
    {
        this.role = roles;
    }

    public boolean isPasswordChanged()
    {
        return passwordChanged;
    }

    public void setPasswordChanged(boolean passwordChanged)
    {
        this.passwordChanged = passwordChanged;
    }

    private Long id;

    @NotNull
    @Size(min = 3, max = 15, message = "{validation.user.name.size}")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "{validation.user.name.pattern}")
    private String name;

    @NotNull
    @Size(min = 5, max = 30, message = "{validation.user.password.size}")
    private String password;

    @NotNull(message = "{validation.user.role}")
    private Role role;

    private boolean passwordChanged;
}
