package mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import service.statistics.StatisticsService;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping({"/home"})
public class StatisticsController
{
	@Autowired
	public StatisticsController(StatisticsService statisticsService)
	{
		this.statisticsService = statisticsService;
	}

	@RequestMapping
	public String showMainPage(Map<String, Object> model)
	{
		updateStatistics(model);

		return "home";
	}

	@RequestMapping(value = "statistics", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Map<String, Object> showMainPage()
	{
		Map<String, Object> statisticsData = new HashMap<>();

		return updateStatistics(statisticsData);
	}

	@RequestMapping(value = "statistics", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void resetStatistics()
	{
		statisticsService.resetStatistics();
	}

	/**
	 * Обновляет отображение, переданное в качестве аргумента, данными статистики для представления.
	 * @param statisticsData отображение, которое необходимо обновить.
	 * @return отображение с обновлёнными данными.
	 */
	private Map<String, Object> updateStatistics(Map<String, Object> statisticsData)
	{
		statisticsData.put("numberOfReadOperations", statisticsService.getTotalReadOperations());
		statisticsData.put("readOperationsInSec", statisticsService.getNumberOfReadOperationsInSec());
		statisticsData.put("numberOfWriteOperations", statisticsService.getTotalWriteOperations());
		statisticsData.put("writeOperationsInSec", statisticsService.getNumberOfWriteOperationsInSec());

		return statisticsData;
	}

	private StatisticsService statisticsService;
}
