package mvc.controller;

import mvc.controller.dto.UserDto;
import mvc.controller.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.admin.AdminService;
import service.admin.domain.User;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class AdminController
{
    @Autowired
    public AdminController(AdminService adminService)
    {
        this.adminService = adminService;
    }

    @RequestMapping
    public String list(Model model)
    {
        List<UserDto> users = adminService.getUsers()
                                          .stream()
                                          .map(user -> convertToUserDto(user))
                                          .collect(Collectors.toList());
        model.addAttribute("users", users);

        return "users";
    }

    @RequestMapping(method = RequestMethod.GET, params = "new")
    public String createForm(Model model)
    {
        return getUserForm(model, new UserDto());
    }

    @RequestMapping(method = RequestMethod.POST, params = "new")
    public String create(@Valid @ModelAttribute("user") UserDto userDto, BindingResult bindingResult, Model model)
    {
        userDto.setPasswordChanged(true);

        return createOrUpdate(bindingResult, model, () -> {
            adminService.addUser(convertToUser(userDto));
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, params = "update")
    public String updateForm(@PathVariable long id, Model model)
    {
        UserDto userDto = getUser(id);

        if(isUserDefaultAdmin(userDto))
        {
            return "redirect:/users";
        }

        return getUserForm(model, userDto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, params = "update")
    public String update(@Valid @ModelAttribute("user") UserDto userDto, BindingResult bindingResult, Model model)
    {
        /*
        TODO: на данный момент значение id в строке запроса ни на что не влияет. Главное - это id у пользователя.
		      Возможно, надо сделать сравнение этих идентификаторов и предпринять какие-то действия.
		 */

        return createOrUpdate(bindingResult, model, () -> {
            adminService.updateUser(convertToUser(userDto));
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, params = "delete")
    public String delete(@PathVariable long id)
    {
        UserDto userDto = getUser(id);

        if(!(isUserDefaultAdmin(userDto)
             || getLoggedUser().getUsername().equalsIgnoreCase(userDto.getName())))
        {
            adminService.deleteUser(convertToUser(userDto));
        }

        return "redirect:/users";
    }

    private String createOrUpdate(BindingResult bindingResult, Model model, Operation op)
    {
        if(bindingResult.hasErrors())
        {
            return getUserForm(model, null);
        }

        try
        {
            op.doIt();
        }
        catch(DataIntegrityViolationException ex)
        {
            bindingResult.addError(new FieldError("user", "name", null,
                                                  false, new String[]{"validation.user.name.duplicate"}, null,
                                                  "Default error message"));
            return getUserForm(model, null);
        }

        return "redirect:/users";
    }

    private String getUserForm(Model model, UserDto userDto)
    {
        if(userDto != null)
        {
            model.addAttribute("user", userDto);
        }
        model.addAttribute("allRoles", adminService.getRoles());

        return "users/profile";
    }

    private boolean isUserDefaultAdmin(UserDto userDto)
    {
        return DEFAULT_ADMIN_NAME.equalsIgnoreCase(userDto.getName());
    }

    private org.springframework.security.core.userdetails.User getLoggedUser()
    {
        return (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private UserDto getUser(long id)
    {
        UserDto userDto = convertToUserDto(adminService.getUser(id));

        if(userDto == null)
        {
            throw new UserNotFoundException();
        }

        return userDto;
    }

    private UserDto convertToUserDto(User user)
    {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setPassword(null);
        userDto.setPasswordChanged(false);
        userDto.setRole(user.getRole());

        return userDto;
    }

    private User convertToUser(UserDto userDto)
    {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setPassword(userDto.isPasswordChanged() ? userDto.getPassword() : "");
        user.setRole(userDto.getRole());

        return user;
    }

    private static final String DEFAULT_ADMIN_NAME = "admin";
    //
    private AdminService adminService;



    @FunctionalInterface
    private interface Operation
    {
        void doIt();
    }
}
