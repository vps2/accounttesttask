INSERT INTO account(id, balance) VALUES(1, 500);
INSERT INTO account(id, balance) VALUES(2, 300);
INSERT INTO account(id, balance) VALUES(3, 200);


# Создание пользователей
INSERT INTO USER(ID, NAME, PASSWORD, ROLE) VALUES(1, 'admin', '$2a$10$wb2QBDhaPFIUcPeSw4iiT.Y/3P1GNPD1gEN8hMTay8pZ.DP8sM8Te', 'ROLE_ADMIN');