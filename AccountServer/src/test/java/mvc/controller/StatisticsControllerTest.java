package mvc.controller;

import org.junit.Before;
import org.junit.Test;
import service.statistics.StatisticsService;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class StatisticsControllerTest
{
	@Before()
	public void setUp()
	{
		service = mock(StatisticsService.class);
		when(service.getTotalReadOperations()).thenReturn(10L);
		when(service.getTotalWriteOperations()).thenReturn(5L);
		when(service.getNumberOfReadOperationsInSec()).thenReturn(100L);
		when(service.getNumberOfWriteOperationsInSec()).thenReturn(50L);

		controller = new StatisticsController(service);
	}

	@Test
	public void test_showMainPageWithModelInArgument() throws Exception
	{
		Map<String, Object> model = new HashMap<>();

		String viewName = controller.showMainPage(model);

		assertEquals("home", viewName);
		//
		assertEquals(10L, model.get("numberOfReadOperations"));
		assertEquals(5L, model.get("numberOfWriteOperations"));
		assertEquals(100L, model.get("readOperationsInSec"));
		assertEquals(50L, model.get("writeOperationsInSec"));
		//
		verify(service, times(1)).getTotalReadOperations();
		verify(service, times(1)).getTotalWriteOperations();
		verify(service, times(1)).getNumberOfReadOperationsInSec();
		verify(service, times(1)).getNumberOfWriteOperationsInSec();
	}

	@Test
	public void test_showMainPageWithoutArguments()
	{
		Map<String, Object> responseData = controller.showMainPage();

		assertEquals(10L, responseData.get("numberOfReadOperations"));
		assertEquals(5L, responseData.get("numberOfWriteOperations"));
		assertEquals(100L, responseData.get("readOperationsInSec"));
		assertEquals(50L, responseData.get("writeOperationsInSec"));
		//
		verify(service, times(1)).getTotalReadOperations();
		verify(service, times(1)).getTotalWriteOperations();
		verify(service, times(1)).getNumberOfReadOperationsInSec();
		verify(service, times(1)).getNumberOfWriteOperationsInSec();
	}

	@Test
	public void test_invokeResetStatistics()
	{
		controller.resetStatistics();

		verify(service, times(1)).resetStatistics();
	}

	private StatisticsService service;
	private StatisticsController controller;
}