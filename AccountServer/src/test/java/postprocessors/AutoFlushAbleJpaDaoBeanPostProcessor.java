package postprocessors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.util.ReflectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * Создаёт прокси над реализациями DAO, который вызывает метод {@link javax.persistence.EntityManager#flush() flush}
 * для модифицирующих операций.
 * <p>
 * При реализации Dao необходимо учитывать следующее:
 *  <ul>
 *     <li>Имя компонента должно состоять из букв и заканчиваться на "Dao", например: <i>AccountDao</i>.</li>
 *     <li>Модифицирующее методы должны называться:
 *     {@link service.common.dao.GenericDao#persist(Object) persist},
 *     {@link service.common.dao.GenericDao#update(Object) update},
 *     {@link service.common.dao.GenericDao#remove(Object) remove}.</li>
 *     <li>Поле, в которое происходит внедрение экземпляра {@link javax.persistence.EntityManager} должно
 *     называться <i>entityManager.</i></li>
 *  </ul>
 *
 */
public class AutoFlushAbleJpaDaoBeanPostProcessor implements BeanPostProcessor
{
	public AutoFlushAbleJpaDaoBeanPostProcessor(JpaDialect jpaDialect)
	{
		this.dialect = jpaDialect;
	}

	public AutoFlushAbleJpaDaoBeanPostProcessor(JpaVendorAdapter jpaVendorAdapter)
	{
		this.dialect = jpaVendorAdapter.getJpaDialect();
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException
	{
		if(beanName.matches("^\\w+Dao$"))
		{
			originalBeans.put(beanName, bean);
		}

		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException
	{
		Object originalBean = originalBeans.get(beanName);
		if(originalBean == null)
		{
			return bean;
		}

		Class<?> beanClass = originalBean.getClass();

		Object proxy = Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
			{
				String methodName = method.getName();
				if("persist".equalsIgnoreCase(methodName)
						     || "update".equalsIgnoreCase(methodName)
						     || "remove".equalsIgnoreCase(methodName))
				{
					EntityManager entityManager = getEntityManager();

					Object retVal = ReflectionUtils.invokeMethod(method, bean, args);
					try
					{
						entityManager.flush();
					}
					catch(PersistenceException ex)
					{
						throw dialect.translateExceptionIfPossible(ex);
					}

					return retVal;
				}
				else
				{
					return ReflectionUtils.invokeMethod(method, bean, args);
				}
			}

			private EntityManager getEntityManager()
			{
				Field field = ReflectionUtils.findField(beanClass, "entityManager");
				ReflectionUtils.makeAccessible(field);
				return (EntityManager) ReflectionUtils.getField(field, originalBean);
			}
		});

		return proxy;
	}

	private Map<String, Object> originalBeans = new HashMap<>();
	private JpaDialect dialect;
}
