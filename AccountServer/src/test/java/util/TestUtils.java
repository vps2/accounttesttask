package util;

import java.util.Collection;

public class TestUtils
{
	public static <T> boolean isCollectionsEquals(Collection<T> a, Collection<T> b)
	{
		return a.size() == b.size() && a.containsAll(b) ;
	}
}
