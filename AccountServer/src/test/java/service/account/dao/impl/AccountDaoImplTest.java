package service.account.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;
import service.account.dao.AccountDao;
import service.account.domain.Account;

import javax.persistence.LockModeType;
import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-context-annotation.xml")
public class AccountDaoImplTest
{
	@Test
	public void test_getAccountWithWrongID()
	{
		final Integer WRONG_ACCOUNT_ID = -1;

		Account actualAccount = accountDao.findByID(WRONG_ACCOUNT_ID, LockModeType.NONE);

		assertNull(actualAccount);
	}

	@Test
	public void test_getAccountWithRightID()
	{
		final Integer RIGHT_ACCOUNT_ID = 0;
		final Long RIGHT_ACCOUNT_BALANCE = 500L;

		Account expectedAccount = new Account();
		expectedAccount.setId(RIGHT_ACCOUNT_ID);
		expectedAccount.deposit(RIGHT_ACCOUNT_BALANCE);

		Account actualAccount = accountDao.findByID(RIGHT_ACCOUNT_ID, LockModeType.NONE);

		assertReflectionEquals(expectedAccount, actualAccount);
	}

	@Test(expected = Exception.class)
	public void test_getAccountWithNullID()
	{
		final Integer WRONG_ACCOUNT_ID = null;

		Account actualAccount = accountDao.findByID(WRONG_ACCOUNT_ID, LockModeType.NONE);

		assertNull(actualAccount);
	}

	@Transactional(transactionManager = "accountServiceTransactionManager")
	@Rollback
	@Test
	public void test_persistNewAccountWithPositiveBalance()
	{
		final Integer ACCOUNT_ID = 3;
		final Long ACCOUNT_BALANCE = 300L;

		Account expectedAccount = new Account();
		expectedAccount.setId(ACCOUNT_ID);
		expectedAccount.deposit(ACCOUNT_BALANCE);

		accountDao.persist(expectedAccount);

		Account actualAccount = accountDao.findByID(ACCOUNT_ID);
		assertReflectionEquals(expectedAccount, actualAccount);
	}

	@Transactional(transactionManager = "accountServiceTransactionManager")
	@Rollback
	@Test(expected = ConstraintViolationException.class)
	public void test_persistNewAccountWithNegativeBalance()
	{
		final Integer ACCOUNT_ID = 3;
		final Long ACCOUNT_BALANCE = -300L;

		Account newAccount = new Account();
		newAccount.setId(ACCOUNT_ID);
		ReflectionTestUtils.setField(newAccount, "balance", ACCOUNT_BALANCE);

		accountDao.persist(newAccount);
	}

	@Transactional(transactionManager = "accountServiceTransactionManager")
	@Rollback
	@Test(expected = DataIntegrityViolationException.class)
	public void test_persistDuplicateAccount()
	{
		final Integer ACCOUNT_ID = 2;
		final Long ACCOUNT_BALANCE = 500L;

		Account newAccount = new Account();
		newAccount.setId(ACCOUNT_ID);
		newAccount.deposit(ACCOUNT_BALANCE);

		accountDao.persist(newAccount);
	}

	@Transactional(transactionManager = "accountServiceTransactionManager")
	@Rollback
	@Test
	public void test_updateExistingAccount()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_BALANCE = 700L;

		Account expectedAccount = new Account();
		expectedAccount.setId(ACCOUNT_ID);
		expectedAccount.deposit(ACCOUNT_BALANCE);

		accountDao.update(expectedAccount);

		Account actualAccount = accountDao.findByID(ACCOUNT_ID);
		assertReflectionEquals(expectedAccount, actualAccount);
	}

	@Transactional(transactionManager = "accountServiceTransactionManager")
	@Rollback
	@Test(expected = ConstraintViolationException.class)
	public void test_updateExistingAccountWithNegativeValueGreaterThanBalance()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_BALANCE = -700L;

		Account expectedAccount = new Account();
		expectedAccount.setId(ACCOUNT_ID);
		ReflectionTestUtils.setField(expectedAccount, "balance", ACCOUNT_BALANCE);

		accountDao.update(expectedAccount);
	}

	@Autowired
	private AccountDao accountDao;
}