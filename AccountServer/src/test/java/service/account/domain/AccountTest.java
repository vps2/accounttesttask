package service.account.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountTest
{
   @Test
   public void test_createNewAccount()
   {
      final long EXPECTED_BALANCE = 0;

      Account account = new Account();

      assertEquals(EXPECTED_BALANCE, account.getBalance());
   }

   @Test
   public void test_depositWithPositiveNumber()
   {
      final long AMOUNT = 100;

      Account account = new Account();

      account.deposit(AMOUNT);

      assertEquals(100, account.getBalance());
   }

   @Test(expected = IllegalArgumentException.class)
   public void test_depositWithZeroShouldThrowException()
   {
      final long AMOUNT = 0;

      Account account = new Account();

      account.deposit(AMOUNT);
   }

   @Test(expected = IllegalArgumentException.class)
   public void test_depositWithNegativeNumberShouldThrowException()
   {
      final long AMOUNT = -100;

      Account account = new Account();

      account.deposit(AMOUNT);
   }

   @Test
   public void test_withdrawFromAccountSufficientAmount()
   {
      final long DEPOSIT_AMOUNT = 100;
      final long WITHDRAW_AMOUNT = 50;
      final long EXPECTED_AMOUNT = 50;

      Account account = new Account();
      account.deposit(DEPOSIT_AMOUNT);

      account.withdraw(WITHDRAW_AMOUNT);

      assertEquals(EXPECTED_AMOUNT, account.getBalance());
   }

   @Test(expected = IllegalArgumentException.class)
   public void test_withdrawFromAccountInsufficientAmountShouldThrowException()
   {
      final long DEPOSIT_AMOUNT = 50;
      final long WITHDRAW_AMOUNT = 100;

      Account account = new Account();
      account.deposit(DEPOSIT_AMOUNT);

      account.withdraw(WITHDRAW_AMOUNT);
   }

   @Test(expected = IllegalArgumentException.class)
   public void test_withdrawFromAccountZeroAmountShouldThrowException()
   {
      final long DEPOSIT_AMOUNT = 100;
      final long WITHDRAW_AMOUNT = 0;

      Account account = new Account();
      account.deposit(DEPOSIT_AMOUNT);

      account.withdraw(WITHDRAW_AMOUNT);
   }
}