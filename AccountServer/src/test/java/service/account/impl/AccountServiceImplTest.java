package service.account.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.account.AccountService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-context-annotation.xml")
public class AccountServiceImplTest
{
	@Test
	public void test_getAmountShouldReturnZeroIfAccountDoesNotExistInDatabase()
	{
		final Integer NON_EXISTENT_ACCOUNT_ID = 3;

		Long actualBalance = accountService.getAmount(NON_EXISTENT_ACCOUNT_ID);

		assertEquals(Long.valueOf(0), actualBalance);
	}

	@Test
	public void test_getAmountOfTheExistingAccount()
	{
		final Integer ACCOUNT_ID = 0;

		Long actualBalance = accountService.getAmount(ACCOUNT_ID);

		assertEquals(Long.valueOf(500), actualBalance);
	}

	@DirtiesContext
	@Test
	public void test_createNewAccountWithPositiveBalance()
	{
		final Integer NEW_ACCOUNT_ID = 3;
		final Long NEW_ACCOUNT_BALANCE = 100L;

		accountService.addAmount(NEW_ACCOUNT_ID, NEW_ACCOUNT_BALANCE);

		Long actualBalance = accountService.getAmount(NEW_ACCOUNT_ID);

		assertEquals(NEW_ACCOUNT_BALANCE, actualBalance);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_createNewAccountWithNegativeBalanceShouldThrowAnException()
	{
		final Integer NEW_ACCOUNT_ID = 3;
		final Long NEW_ACCOUNT_BALANCE = -100L;

		accountService.addAmount(NEW_ACCOUNT_ID, NEW_ACCOUNT_BALANCE);
	}

	@DirtiesContext
	@Test
	public void test_addingAmountToExistingAccount()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_SUM_FOR_ADDING = 30L;

		accountService.addAmount(ACCOUNT_ID, ACCOUNT_SUM_FOR_ADDING);

		Long actualBalance = accountService.getAmount(ACCOUNT_ID);

		assertEquals(Long.valueOf(530), actualBalance);
	}

	@DirtiesContext
	@Test
	public void test_addingAmountWithNegativeValueToExistingAccountShouldReduceBalance()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_SUM_FOR_ADDING = -30L;

		accountService.addAmount(ACCOUNT_ID, ACCOUNT_SUM_FOR_ADDING);

		Long actualBalance = accountService.getAmount(ACCOUNT_ID);

		assertEquals(Long.valueOf(470), actualBalance);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_addingAmountWithNegativeValueGreaterThanBalanceOfExistingAccountShouldThrowException()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_SUM_FOR_ADDING = -700L;

		accountService.addAmount(ACCOUNT_ID, ACCOUNT_SUM_FOR_ADDING);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_addingZeroAmountToAnExistingAccountShouldThrowException()
	{
		final Integer ACCOUNT_ID = 0;
		final Long ACCOUNT_SUM_FOR_ADDING = 0L;

		accountService.addAmount(ACCOUNT_ID, ACCOUNT_SUM_FOR_ADDING);
	}

	@Autowired
	private AccountService accountService;
}