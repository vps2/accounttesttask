package service.admin.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.admin.AdminService;
import service.admin.domain.Role;
import service.admin.domain.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static util.TestUtils.isCollectionsEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-context-annotation.xml")
public class AdminServiceImplTest
{
	@Test
	public void test_GetAllUsers()
	{
		User admin = new User();
		admin.setId(0L);
		admin.setName("admin");
		admin.setPassword("admin");
		admin.setRole(Role.ROLE_ADMIN);
		//
		User user = new User();
		user.setId(1L);
		user.setName("user");
		user.setPassword("password");
		user.setRole(Role.ROLE_USER);

		List<User> expectedUsers = new ArrayList<>(Arrays.asList(user, admin));

		List<User> users = adminService.getUsers();

		String errorMessage = String.format("Collections are not equals:\n Expected: %s\n Actual: %s\n"
				  , expectedUsers
				  , users);
		assertTrue(errorMessage, isCollectionsEquals(expectedUsers, users));
	}

	@Transactional(transactionManager = "adminServiceTransactionManager")
	@Rollback
	@Test
	public void test_UpdateUser()
	{
		final long USER_ID = 0;
		//
		User expectedUser = new User();
		expectedUser.setId(USER_ID);
		expectedUser.setName("new_name");
		expectedUser.setPassword("new_password");
		expectedUser.setRole(Role.ROLE_USER);

		adminService.updateUser(expectedUser);

		User userFromStorage = adminService.getUser(USER_ID);

		assertReflectionEquals(expectedUser, userFromStorage);
	}

	@Transactional(transactionManager = "adminServiceTransactionManager")
	@Rollback
	@Test
	public void test_updateUserWithBlankPasswordFieldLeaveOldPassword()
	{
		final long USER_ID = 0;
		//
		User originalUserFromStorage = adminService.getUser(USER_ID);
		String originalUserPassword = originalUserFromStorage.getPassword();

		User updatedUser = new User();
		updatedUser.setId(USER_ID);
		updatedUser.setName("admin");
		updatedUser.setPassword("");
		updatedUser.setRole(Role.ROLE_ADMIN);

		adminService.updateUser(updatedUser);

		User updatedUserFromStorage = adminService.getUser(USER_ID);

		assertReflectionEquals(originalUserPassword, updatedUserFromStorage.getPassword());
	}

	@Transactional(transactionManager = "adminServiceTransactionManager")
	@Rollback
	@Test
	public void test_DeleteUser()
	{
		User user = adminService.getUser(0L);
		//
		adminService.deleteUser(user);

		User userFormStorage = adminService.getUser(user.getId());

		assertNull(userFormStorage);
	}

	@Autowired
	private AdminService adminService;
}