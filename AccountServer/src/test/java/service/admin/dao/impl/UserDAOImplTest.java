package service.admin.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.admin.dao.UserDAO;
import service.admin.domain.Role;
import service.admin.domain.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static util.TestUtils.isCollectionsEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-context-annotation.xml")
public class UserDAOImplTest
{
   @Test
   public void test_GetAllUsers()
   {
      List<User> expectedUsers = new ArrayList<User>()
      {
         {
            User dbUser1 = new User();
            dbUser1.setId(1L);
            dbUser1.setName("user");
            dbUser1.setPassword("password");
            dbUser1.setRole(Role.ROLE_USER);

            User dbUser2 = new User();
            dbUser2.setId(0L);
            dbUser2.setName("admin");
            dbUser2.setPassword("admin");
            dbUser2.setRole(Role.ROLE_ADMIN);

            add(dbUser1);
            add(dbUser2);
         }
      };

      List<User> users = userDAO.findAll();

      String errorMessage = String.format("Collections are not equals:\n Expected: %s\n Actual: %s\n"
              , expectedUsers
              , users);
      assertTrue(errorMessage, isCollectionsEquals(expectedUsers, users));
   }

   @Test
   public void test_GetNonExistentUser()
   {
      final long USER_WITH_WRONG_ID = -1;

      User user = userDAO.findByID(USER_WITH_WRONG_ID);

      assertNull(user);
   }

   @Test
   public void test_GetUserWithRightID()
   {
      final long USER_WITH_RIGHT_ID = 0;

      User expectedUser = new User();
      expectedUser.setId(0L);
      expectedUser.setName("admin");
      expectedUser.setPassword("admin");
      expectedUser.setRole(Role.ROLE_ADMIN);

      User user = userDAO.findByID(USER_WITH_RIGHT_ID);

      assertReflectionEquals(expectedUser, user);
   }

   @Transactional(transactionManager = "adminServiceTransactionManager")
   @Rollback
   @Test
   public void test_InsertNewUser()
   {
      User newUser = new User();
      newUser.setName("new_user");
      newUser.setPassword("user_password");
      newUser.setRole(Role.ROLE_USER);

      userDAO.persist(newUser);

      User userFormStorage = userDAO.findByID(newUser.getId());

      assertReflectionEquals(newUser, userFormStorage);
   }

   @Transactional(transactionManager = "adminServiceTransactionManager")
   @Rollback
   @Test(expected = DataIntegrityViolationException.class)
   public void test_InsertNewUserWithDuplicateName()
   {
      User newUser = new User();
      newUser.setName("user");
      newUser.setPassword("user_password");
      newUser.setRole(Role.ROLE_USER);

      userDAO.persist(newUser);
   }

   @Transactional(transactionManager = "adminServiceTransactionManager")
   @Rollback
   @Test
   public void test_updateUser()
   {
      User userForUpdate = userDAO.findByID(1L);
      userForUpdate.setName("new_name");
      userForUpdate.setPassword("new_password");
      userForUpdate.setRole(Role.ROLE_ADMIN);

      userDAO.update(userForUpdate);

      User updatedUserFromStorage = userDAO.findByID(1L);

      assertReflectionEquals(userForUpdate, updatedUserFromStorage);
   }

   @Transactional(transactionManager = "adminServiceTransactionManager")
   @Test(expected = DataIntegrityViolationException.class)
   public void test_updateWithTryToInsertDuplicateName()
   {
      User user = userDAO.findByID(1L);
      user.setName("admin");

      userDAO.update(user);
   }

   @Transactional(transactionManager = "adminServiceTransactionManager")
   @Rollback
   @Test
   public void test_deleteUser()
   {
      final long USER_ID = 1;
      User user = userDAO.findByID(USER_ID);

      userDAO.remove(user);

      User userFromStorage = userDAO.findByID(USER_ID);

      assertNull(userFromStorage);
   }

   @Autowired
   private UserDAO userDAO;
}