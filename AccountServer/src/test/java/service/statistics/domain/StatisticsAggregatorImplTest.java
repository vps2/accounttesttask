package service.statistics.domain;

import org.junit.Before;
import org.junit.Test;
import service.statistics.StatisticsAggregator;
import service.statistics.impl.StatisticsAggregatorImpl;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class StatisticsAggregatorImplTest
{
	@Before
	public void setUp()
	{
		spyAggregator = spy(new StatisticsAggregatorImpl());
	}

	@Test
	public void test_ThreadSafe_UpdateNumberOfReadOperationsMethodInvocation() throws InterruptedException
	{
		ThreadGroup threadGroup = new ThreadGroup("aggregatorClients");
		CountDownLatch latch = new CountDownLatch(1);
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfReadOperations, latch)).start();
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfReadOperations, latch)).start();
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfReadOperations, latch)).start();

		//разрешаем параллельную работу потоков
		latch.countDown();
		//Ожидаем завершение работы потоков
		while(threadGroup.activeCount() > 0)
		{
		}

		assertEquals(3, spyAggregator.getTotalReadOperations());
		verify(spyAggregator, times(3)).updateNumberOfReadOperations();
	}

	@Test
	public void test_ThreadSafe_UpdateNumberOfWriteOperationsMethodInvocation() throws InterruptedException
	{
		ThreadGroup threadGroup = new ThreadGroup("aggregatorClients");
		CountDownLatch latch = new CountDownLatch(1);
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfWriteOperations, latch)).start();
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfWriteOperations, latch)).start();
		new Thread(threadGroup, new MethodInvoker(spyAggregator::updateNumberOfWriteOperations, latch)).start();

		//разрешаем параллельную работу потоков
		latch.countDown();
		//Ожидаем завершение работы потоков
		while(threadGroup.activeCount() > 0)
		{
		}

		assertEquals(3, spyAggregator.getTotalWriteOperations());
		verify(spyAggregator, times(3)).updateNumberOfWriteOperations();
	}

	@Test
	public void test_ResetStatistics()
	{
		final int NUMBER_OF_UPDATE_OPERATIONS = 3;
		for(int index = 0; index < NUMBER_OF_UPDATE_OPERATIONS; ++index)
		{
			spyAggregator.updateNumberOfReadOperations();
			spyAggregator.updateNumberOfWriteOperations();
		}

		spyAggregator.resetStatistics();

		assertEquals(0, spyAggregator.getTotalReadOperations());
		assertEquals(0, spyAggregator.getTotalWriteOperations());
		//
		verify(spyAggregator, times(3)).updateNumberOfReadOperations();
		verify(spyAggregator, times(3)).updateNumberOfWriteOperations();
		verify(spyAggregator).resetStatistics();
	}

	private StatisticsAggregator spyAggregator;



	private static class MethodInvoker implements Runnable
	{
		public MethodInvoker(ClassMethod method, CountDownLatch latch)
		{
			this.method = method;
			this.latch = latch;
		}

		@Override
		public void run()
		{
			try
			{
				latch.await();
				method.invoke();
			}
			catch(InterruptedException ex)
			{
				ex.printStackTrace();
			}
		}

		private ClassMethod method;
		private CountDownLatch latch;
	}

	private interface ClassMethod
	{
		void invoke();
	}
}