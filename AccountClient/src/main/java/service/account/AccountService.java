package service.account;

public interface AccountService
{
	/**
	 * Возвращает текущий баланс для заданного id.
	 * @param id идентификатор баланса
	 * @return баланс или 0, если метод addAmount() ещё не вызывался для данного id
	 */
	Long getAmount(Integer id);

	/**
	 * Увеличивает баланс или устанавливает в начальное значение, если метод addAmount() для заданного id ещё не вызывался.
	 * @param id идентификатор баланса
	 * @param value значение баланса (может быть отрицательным), которое должно быть прибавлено к текущему.
	 */
	void addAmount(Integer id, Long value);
}
