import client.ClientOperation;
import client.HessianClient;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class Solution
{
	public static void main(String[] args) throws IOException
	{
		Options options = new Options();
		options.addOption(Option.builder("i")
				                    .longOpt("idle")
				                    .desc("the waiting time (in seconds) before program exit")
				                    .hasArg()
				                    .argName("SECONDS")
				                    .required()
				                    .build());
		CommandLine cmd = null;
		int idle = 0;
		try
		{
			cmd = new DefaultParser().parse(options, args);
			idle = Integer.valueOf(cmd.getOptionValue("idle"));
		}
		catch(ParseException | NumberFormatException ex)
		{
			HelpFormatter helpFormatter = new HelpFormatter();
			helpFormatter.printHelp("client-1.0-SNAPSHOT.jar", options);
			return;
		}

		Solution solution = new Solution();
		solution.start();

		final int ONE_SECOND_FACTOR = 1000;
		try
		{
			Thread.currentThread().sleep(idle * ONE_SECOND_FACTOR);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}

		solution.stop();
	}

	public void start() throws IOException
	{
		Properties props = loadProperties(configFile);

		int numbersOfReaders = Integer.parseInt(props.getProperty("readers", DEFAULT_NUMBER_OF_READERS));
		int numberOfWriters = Integer.parseInt(props.getProperty("writers", DEFAULT_NUMBER_OF_WRITERS));
		url = props.getProperty("url", DEFAULT_URL);
		String keysAsString = props.getProperty("keys", DEFAULT_KEYS);
		for(String key : keysAsString.split(","))
		{
			keys.add(Integer.parseInt(key));
		}

		latch = new CountDownLatch(numbersOfReaders + numberOfWriters);

		createThreads(ClientOperation.GET_AMOUNT, numbersOfReaders, threadGroup);
		createThreads(ClientOperation.ADD_MOUNT, numberOfWriters, threadGroup);
	}

	private void stop()
	{
		threadGroup.interrupt();
	}

	private Properties loadProperties(String fileName) throws IOException
	{
		Properties props = new Properties();

		try(InputStream in = Solution.class.getResourceAsStream(fileName))
		{
			props.load(in);
		}

		return props;
	}

	private void createThreads(ClientOperation clientOperation, int numberOfThreads, ThreadGroup threadGroup)
	{
		for(int threadNumber = 0; threadNumber < numberOfThreads; ++threadNumber)
		{
			Runnable hessianClient =
					  new HessianClient.HessianClientBuilder(url, keys, clientOperation).latch(latch).build();
			new Thread(threadGroup, hessianClient).start();
			latch.countDown();
		}
	}

	private static final String DEFAULT_NUMBER_OF_READERS = "1";
	private static final String DEFAULT_NUMBER_OF_WRITERS = "1";
	private static final String DEFAULT_KEYS = "1,2,3";
	private static final String DEFAULT_URL = "http://localhost:8080/AccountService";
	//
	private static final String configFile = "settings/config.properties";
	//
	private ThreadGroup threadGroup = new ThreadGroup("group");
	private List<Integer> keys = new ArrayList<>();
	private String url;
	private CountDownLatch latch;
}
