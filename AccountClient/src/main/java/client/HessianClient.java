package client;

import com.caucho.hessian.client.HessianProxyFactory;
import service.account.AccountService;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

public class HessianClient implements Runnable
{
   @Override
   public void run()
   {
      AccountService accountService = null;

      try
      {
         accountService = getAccountServiceProxy();
         if(latch != null)
         {
            latch.await();
         }
      }
      catch(MalformedURLException | InterruptedException ex)
      {
         ex.printStackTrace();
         Thread.currentThread().interrupt();
      }

      while(!Thread.currentThread().isInterrupted())
      {
         int index = ThreadLocalRandom.current().nextInt(accountsIDs.size());

         Integer accountID = accountsIDs.get(index);

         switch(clientOperation)
         {
            case GET_AMOUNT:
            {
               getAmount(accountService, accountID);

               break;
            }
            case ADD_MOUNT:
            {
               long amount = nextValue();

               try
               {
                  addAmount(accountService, accountID, amount);
               }
               catch(IllegalArgumentException ex)
               {
                  String format = "%s\tOperation 'addAmount(%d,%d) was interrupted.";
                  System.err.println(String.format(format, Thread.currentThread(), accountID, amount));
               }

               break;
            }
         }
         Thread.yield();
      }
   }

   private HessianClient(HessianClientBuilder builder)
   {
      HESSIAN_SERVICE_URL = builder.serviceUrl;
      this.accountsIDs = builder.keys;
      this.clientOperation = builder.clientOperation;
      this.latch = builder.latch;
   }

   private AccountService getAccountServiceProxy() throws MalformedURLException
   {
      HessianProxyFactory hessianProxyFactory = new HessianProxyFactory();
      return (AccountService) hessianProxyFactory.create(AccountService.class, HESSIAN_SERVICE_URL);
   }

   private long nextValue()
   {
      long value;
      do
      {
         value = ThreadLocalRandom.current().nextLong(MIN_BOUND, MAX_BOUND);
      }
      while(value == 0);

      return value;
   }

   private long getAmount(AccountService accountService, Integer accountID)
   {
//      System.err.println("Thread: " + Thread.currentThread() +
//                         ". Get amount for account with id = " + accountID + ". Value = " + accountService.getAmount(accountID));
      return accountService.getAmount(accountID);
   }

   private void addAmount(AccountService accountService, Integer accountID, Long amount)
   {
//		System.err.println("Thread: " + Thread.currentThread() +
//								 ". Add amount for account with id = " + accountID + ". Adding the value = " + amount);
      accountService.addAmount(accountID, amount);
   }

   private final static Long MIN_BOUND = -10L;
   private final static Long MAX_BOUND = 11L;
   //
   private final String HESSIAN_SERVICE_URL;
   private final ClientOperation clientOperation;
   private final List<Integer> accountsIDs;
   private final CountDownLatch latch;



   public static class HessianClientBuilder
   {
      public HessianClientBuilder(String serviceUrl, List<Integer> keys, ClientOperation clientOperation)
      {
         this.serviceUrl = serviceUrl;
         this.keys = new ArrayList<>(keys);
         this.clientOperation = clientOperation;
      }

      public HessianClientBuilder latch(CountDownLatch latch)
      {
         this.latch = latch;
         return this;
      }

      public HessianClient build()
      {
         return new HessianClient(this);
      }

      //Обязательные параметры
      private final String serviceUrl;
      private final List<Integer> keys;
      private final ClientOperation clientOperation;
      //Необязательные параметры
      private CountDownLatch latch;
   }
}
